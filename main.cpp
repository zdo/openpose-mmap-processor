#include <gflags/gflags.h>

// Allow Google Flags in Ubuntu 14
#ifndef GFLAGS_GFLAGS_H_
    namespace gflags = google;
#endif

#include <openpose/core/headers.hpp>
#include <openpose/filestream/headers.hpp>
#include <openpose/gui/headers.hpp>
#include <openpose/pose/headers.hpp>
#include <openpose/utilities/headers.hpp>

#include <signal.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

static bool stopReading = false;

DEFINE_int32(logging_level,             3,              "The logging level. Integer in the range [0, 255]. 0 will output any log() message, while"
                                                        " 255 will not output any. Current OpenPose library messages are in the range 0-4: 1 for"
                                                        " low priority messages and 4 for important ones.");

DEFINE_string(camera, "/tmp/camera", "Path to camera frame file");
DEFINE_string(output, "/tmp/openpose-mmap-output", "Path to output file");
DEFINE_int32(maxCameraSize, 4096, "Max camera frame size (will downscale if it's bigger than specified value)");
DEFINE_bool(debugRender, false, "Enable debug render");
DEFINE_double(sleep, 0.0f, "Sleep between iterations");
DEFINE_bool(result01, false, "Return results in interval 0..1 instead of 0..width and 0..height");

DEFINE_string(model_folder,             "/app/openpose/models/",      "Folder path (absolute or relative) where the models (pose, face, ...) are located.");
DEFINE_string(net_resolution,           "-1x368",       "Multiples of 16. If it is increased, the accuracy potentially increases. If it is"
                                                        " decreased, the speed increases. For maximum speed-accuracy balance, it should keep the"
                                                        " closest aspect ratio possible to the images or videos to be processed. Using `-1` in"
                                                        " any of the dimensions, OP will choose the optimal aspect ratio depending on the user's"
                                                        " input value. E.g. the default `-1x368` is equivalent to `656x368` in 16:9 resolutions,"
                                                        " e.g. full HD (1980x1080) and HD (1280x720) resolutions.");
DEFINE_string(output_resolution,        "-1x-1",        "The image resolution (display and output). Use \"-1x-1\" to force the program to use the"
                                                        " input image resolution.");
DEFINE_int32(num_gpu_start,             0,              "GPU device start number.");
DEFINE_double(scale_gap,                0.3,            "Scale gap between scales. No effect unless scale_number > 1. Initial scale is always 1."
                                                        " If you want to change the initial scale, you actually want to multiply the"
                                                        " `net_resolution` by your desired initial scale.");
DEFINE_int32(scale_number,              1,              "Number of scales to average.");


// OpenPose Rendering
DEFINE_bool(disable_blending,           false,          "If enabled, it will render the results (keypoint skeletons or heatmaps) on a black"
                                                        " background, instead of being rendered into the original image. Related: `part_to_show`,"
                                                        " `alpha_pose`, and `alpha_pose`.");
DEFINE_double(render_threshold,         0.05,           "Only estimated keypoints whose score confidences are higher than this threshold will be"
                                                        " rendered. Generally, a high threshold (> 0.5) will only render very clear body parts;"
                                                        " while small thresholds (~0.1) will also output guessed and occluded keypoints, but also"
                                                        " more false positives (i.e. wrong detections).");
DEFINE_double(alpha_pose,               0.6,            "Blending factor (range 0-1) for the body part rendering. 1 will show it completely, 0 will"
                                                        " hide it. Only valid for GPU rendering.");

struct __attribute__((packed, aligned(1))) OpenposeHumanData {
    float keypoints[25 * 3];
};

struct __attribute__((packed, aligned(1))) OpenposeFile {
    uint32_t version;
    uint32_t frameIndex;
    uint32_t peopleCount;
    OpenposeHumanData humans[16];
};

static void signalHandler(int nSignalNumber)
{
    printf("Signal %d caught\n", nSignalNumber);
    stopReading = true;
}

static char * openMmapFile(const std::string &path, size_t size, bool readonly, int *file)
{
    if (readonly) {
        *file = open(path.c_str(), O_RDONLY);
        if (*file < 0) {
            throw std::runtime_error("Error occured during opening the readonly file \"" + path + "\"");
        }

        struct stat statinfo;
        fstat(*file, &statinfo);
        size = statinfo.st_size;
    } else {
        *file = open(path.c_str(), O_RDWR | O_CREAT | O_TRUNC, (mode_t)0644);
        if (*file < 0) {
            throw std::runtime_error("Error occured during opening the file \"" + path + "\"");
        }

        char *zeros = (char *)calloc(1, size);
        write(*file, zeros, size);
        free(zeros);
    }

    char *mappedData = (char *)mmap(NULL, size, readonly ? PROT_READ : PROT_READ | PROT_WRITE,
        MAP_SHARED, *file, 0);
    if (mappedData == MAP_FAILED) {
        throw std::runtime_error("Error occured during memory mapping the file \"" + path + "\"");
    }

    return mappedData;
}

bool readFrameFromCamera(uint32_t *lastReadFrame, const uint32_t *cameraData, cv::Mat &image)
{
    uint32_t newFrame = cameraData[0];
    if (newFrame == *lastReadFrame) {
        return false;
    }

    uint32_t width = cameraData[1];
    uint32_t height = cameraData[2];

    int wantedWidth = std::min((int)width, FLAGS_maxCameraSize);
    float aspect = (float)width / height;
    int wantedHeight = wantedWidth / aspect;
    float scale = (float)height / wantedHeight;

    //printf("Frame %dx%d -> %dx%d\n", width, height, wantedWidth, wantedHeight);

    cv::Mat origImage = cv::Mat(height, width, CV_8UC3);
    memcpy(origImage.data, cameraData + sizeof(uint32_t) * 3, width * height * 3);
    cv::resize(origImage, image, cv::Size(wantedWidth, wantedHeight), 0, 0, cv::INTER_LINEAR);

    *lastReadFrame = newFrame;

    return true;
}

int doTheJob()
{
    try
    {
        op::log("Starting OpenPose mmap processor...", op::Priority::High);

        // Setup logger.
        op::check(0 <= FLAGS_logging_level && FLAGS_logging_level <= 255, "Wrong logging_level value.",
                  __LINE__, __FUNCTION__, __FILE__);
        op::ConfigureLog::setPriorityThreshold((op::Priority)FLAGS_logging_level);
        op::log("", op::Priority::Low, __LINE__, __FUNCTION__, __FILE__);

        // Setup camera reader.
        int cameraFile = 0;
        const uint32_t *cameraData = (const uint32_t *)openMmapFile(FLAGS_camera, 0, true, &cameraFile);

        int outputFd = 0;
        size_t outputDataSize = 1000 * 10;
        void *outputData = openMmapFile(FLAGS_output, outputDataSize, false, &outputFd);
        OpenposeFile *outputFile = (OpenposeFile *)outputData;
        outputFile->version = 1;
        msync(outputData, 4, 0);
        
        const auto outputSize = op::flagsToPoint(FLAGS_output_resolution, "-1x-1");
        const auto netInputSize = op::flagsToPoint(FLAGS_net_resolution, "-1x368");
        const auto poseModel = op::flagsToPoseModel("BODY_25");

        op::ScaleAndSizeExtractor scaleAndSizeExtractor(netInputSize, outputSize, FLAGS_scale_number, FLAGS_scale_gap);
        op::CvMatToOpInput cvMatToOpInput{poseModel};
        op::CvMatToOpOutput cvMatToOpOutput;
        op::PoseExtractorCaffe poseExtractorCaffe{poseModel, FLAGS_model_folder, FLAGS_num_gpu_start};
        
        op::PoseCpuRenderer poseRenderer{poseModel, (float)FLAGS_render_threshold, !FLAGS_disable_blending,
                                         (float)FLAGS_alpha_pose};
        op::FrameDisplayer frameDisplayer{"OpenPose mmap processor", outputSize};
        op::OpOutputToCvMat opOutputToCvMat;

        poseExtractorCaffe.initializationOnThread();

        const bool shouldRender = FLAGS_debugRender;
        if (shouldRender) {
            poseRenderer.initializationOnThread();
        }

        uint32_t lastReadFrame = 0;
        cv::Mat inputImage;

        op::log("Start reading frames...", op::Priority::High);

        while (!stopReading) {
            bool shouldProcess = readFrameFromCamera(&lastReadFrame, cameraData, inputImage);
            if (!shouldProcess) {
                // printf("Skip frame %d\n", (int)lastReadFrame);
                usleep(1.0f / 60.0f * 1000000);
                continue;
            }

            const auto timerBegin = std::chrono::high_resolution_clock::now();
        
            const op::Point<int> imageSize{inputImage.cols, inputImage.rows};
            
            std::vector<double> scaleInputToNetInputs;
            std::vector<op::Point<int>> netInputSizes;
            double scaleInputToOutput;
            op::Point<int> outputResolution;
            std::tie(scaleInputToNetInputs, netInputSizes, scaleInputToOutput, outputResolution)
                = scaleAndSizeExtractor.extract(imageSize);

            // Step 3 - Format input image to OpenPose input and output formats
            const auto netInputArray = cvMatToOpInput.createArray(inputImage, scaleInputToNetInputs, netInputSizes);
            auto outputArray = cvMatToOpOutput.createArray(inputImage, scaleInputToOutput, outputResolution);

            // Step 4 - Estimate poseKeypoints
            poseExtractorCaffe.forwardPass(netInputArray, imageSize, scaleInputToNetInputs);
            const auto poseKeypoints = poseExtractorCaffe.getPoseKeypoints();
            // printf("poseKeypoints len=%s\n", poseKeypoints.printSize().c_str());


            outputFile->version = 1;
            outputFile->frameIndex = cameraData[0]; // frame index

            outputFile->peopleCount = poseKeypoints.getSize(0);

            for (int personIndex = 0; personIndex < poseKeypoints.getSize(0); ++personIndex) {
                assert(poseKeypoints.getSize(1) == 25);

                for (int i = 0; i < 25; ++i) {
                    size_t offset = i * 3;
                    float mulW = 1.0f, mulH = 1.0f;
                    if (FLAGS_result01) {
                        mulW = 1.0f / inputImage.cols;
                        mulH = 1.0f / inputImage.rows;
                    }
                    outputFile->humans[personIndex].keypoints[offset++] = poseKeypoints[{personIndex, i, 0}] * mulW;
                    outputFile->humans[personIndex].keypoints[offset++] = poseKeypoints[{personIndex, i, 1}] * mulH;
                    outputFile->humans[personIndex].keypoints[offset++] = poseKeypoints[{personIndex, i, 2}];
                }
            }
            msync(outputData, outputDataSize, 0);
            
            const auto now = std::chrono::high_resolution_clock::now();
            const auto totalTimeSec = (double)std::chrono::duration_cast<std::chrono::nanoseconds>(now-timerBegin).count()
                                    * 1e-9;
            const auto message = "Frame processed for " + std::to_string(totalTimeSec) + " seconds " +
                " frame=" + std::to_string(outputFile->frameIndex) +
                " persons=" + std::to_string(outputFile->peopleCount);
            op::log(message, op::Priority::High);

            if (shouldRender) {
                poseRenderer.renderPose(outputArray, poseKeypoints, scaleInputToOutput);
                auto outputImage = opOutputToCvMat.formatToCvMat(outputArray);
                frameDisplayer.displayFrame(outputImage, 25);
            }

            if (FLAGS_sleep > 0.0f) {
                usleep(FLAGS_sleep * 1000000.0f);
            }
        }
        
        return 0;
    }
    catch (const std::exception& e)
    {
        op::error(e.what(), __LINE__, __FUNCTION__, __FILE__);
        return -1;
    }
}

int main(int argc, char *argv[])
{
    struct sigaction signalAction;
    memset(&signalAction, 0, sizeof(signalAction));
    signalAction.sa_handler = signalHandler;
    sigaction(SIGTERM, &signalAction, NULL);
    sigaction(SIGINT, &signalAction, NULL);

    gflags::ParseCommandLineFlags(&argc, &argv, true);
    return doTheJob();
}
