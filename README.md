# OpenPose mmap processor

There is an utility that does the following:

- it reads camera frame from mmap-ed file (`--camera <file>`)
- it finds poses and writes it to file `--output <outfile>`

## Build

```bash
./docker-build
```

## Usage

```bash
nvidia-docker run -ti neuromationorg/openpose-mmap-processor --camera /tmp/openpose-camera --output /tmp/openpose-output
```

Add `--debugRender` and `export DISPLAY=:1` to see what's going on inside.

## Output file format

```
- uint32 - format version (1)
- uint32 - frame index
- uint32 - number of people

# info about person
- 25 * 3 * 4 байта - 25 points (XYW) in BODY_25 format, float32
```